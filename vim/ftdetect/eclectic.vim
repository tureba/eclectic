" Vim filetype detection file
" Language:     eclectic module
" Author:       Saleem Abdulrasool <compnerd@compnerd.org>
" Copyright:    Copyright © 2012 Saleem Abdulrasool <compnerd@compnerd.org>
" Version:      $Revision$

" Determine if we need to execute {{{
if &compatible || v:version < 603
    finish
endif
" }}}

au BufNewFile,BufRead *.eclectic setl ft=eclectic

" vim: set et fdm=marker fmr={{{,}}} ft=vim sts=4 sw=4 ts=8:

