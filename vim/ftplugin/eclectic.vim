" Vim filetype plugin file
" Language:     eclectic module
" Author:       Saleem Abdulrasool <compnerd@compnerd.org>
" Copyright:    Copyright © 2012 Saleem Abdulrasool <compnerd@compnerd.org>
" Version:      $Revision$

" Determine if we need to execute {{{
if &compatible || v:version < 603
    finish
endif
" }}}

runtime! ftplugin/sh.vim

setlocal expandtab
setlocal fileencoding=utf-8
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=8
setlocal textwidth=80

" vim: set et fdm=marker fmr={{{,}}} ft=vim sts=4 sw=4 ts=8:

